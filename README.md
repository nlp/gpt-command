# Dialog for Pepper to run commands through ChatGPT

Developed at [NLP Centre](https://nlp.fi.muni.cz/en), [FI MU](https://www.fi.muni.cz/index.html.en) for [Karel Pepper](https://nlp.fi.muni.cz/projects/pepper)

The application is to be installed from the `gpt-command-*.pkg` file.

The application contains a dialog in English and Czech for a command send to ChatGPT.

Start with "Karle, pojďme něco vymyslet | Karel let's create something".

See `dlg_gpt-command/dlg_gpt-command_enu.top` or `dlg_gpt-command/dlg_gpt-command_enu.top` for ways of formulating the command.

## Installation

* setup:
    - add `OPENAI_API_KEY` to `gpt_cgi.py`
    - copy or link `gpt_cgi.py` to a directory accessible with a `http://...` URL and runnable as a CGI script with 
      writeable access to `/var/log/gpt_command.log`.
      
      `gpt_flask.py` is an alternative with a standalone server which is currently not used (and not needed).
    - store the `gpt_cgi.py` URL in the `GPT_SERVICE_URL` variable in `scripts/gpt_service.py`
* [make and install](https://nlp.fi.muni.cz/trac/pepper/wiki/InstallPkg) the package as usual for the Pepper robot
