<?xml version="1.0" encoding="UTF-8" ?>
<Package name="gpt-command" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions/>
    <Dialogs>
        <Dialog name="dlg_gpt-command" src="dlg_gpt-command/dlg_gpt-command.dlg" />
    </Dialogs>
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="gpt_service" src="scripts/gpt_service.py" />
        <File name="__init__" src="scripts/stk/__init__.py" />
        <File name="events" src="scripts/stk/events.py" />
        <File name="logging" src="scripts/stk/logging.py" />
        <File name="runner" src="scripts/stk/runner.py" />
        <File name="services" src="scripts/stk/services.py" />
    </Resources>
    <Topics>
        <Topic name="dlg_gpt-command_czc" src="dlg_gpt-command/dlg_gpt-command_czc.top" topicName="dlg_gpt-command" language="cs_CZ" />
        <Topic name="dlg_gpt-command_enu" src="dlg_gpt-command/dlg_gpt-command_enu.top" topicName="dlg_gpt-command" language="en_US" />
    </Topics>
    <IgnoredPaths />
</Package>
