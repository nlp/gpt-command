#!/usr/bin/python
# -*- coding: utf-8 -*-
#
"""
A sample showing how to have a NAOqi service as a Python app.
"""

__version__ = "0.0.1"

__copyright__ = "Copyright (C) 2024, NLP Centre, FI MU"
__author__ = 'Jakub Semeniuk, Ales Horak'
__email__ = '514217@muni.cz, hales@fi.muni.cz'

GPT_SERVICE_URL = "https://..."

TIMEOUT = 10

import qi

import stk.runner
import stk.events
import stk.services
import stk.logging

import math
import re
import sys
import random
import requests
import random

# solution for mismatched SSL certificate of https://www.idsjmk.cz
import urllib3.contrib.pyopenssl
import certifi
import urllib3

urllib3.contrib.pyopenssl.inject_into_urllib3()
urllib3.disable_warnings()
#http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())
http = urllib3.PoolManager(cert_reqs='CERT_NONE',ca_certs=certifi.where())

DEBUG_CONVERT = False
#DEBUG_CONVERT = True

SERVICE_PROMPTS = {
    'Czech': {
        'sending_prompt': [
            'Musím se zamyslet.',
            'Jak to jenom...',
            'Počkej chvilku',
        ],
        'received_prompt': [
            'Už to mám!',
            'Tady to je!',
        ],
    },
    'English': {
        'sending_prompt': [
            'Just a moment, thinking.',
        ],
        'received_prompt': [
            'Here it is!',
        ],
    },
}

class ALGPTService(object):
    "NAOqi service example (set/get on a simple value)."
    APP_ID = "com.aldebaran.ALGPTService"
    def __init__(self, qiapp):
        self.language = 'Czech'
        if DEBUG_CONVERT:
            import logging
            self.logger = logging.getLogger()
            self.logger.setLevel(logging.DEBUG)
            handler = logging.StreamHandler(sys.stdout)
            handler.setLevel(logging.DEBUG)
            self.logger.addHandler(handler)
        else:
            self.qiapp = qiapp
            self.events = stk.events.EventHelper(qiapp.session)
            self.s = stk.services.ServiceCache(qiapp.session)
            self.logger = stk.logging.get_logger(qiapp.session, self.APP_ID)
            self.language = self.s.ALDialog.getLanguage()

    def say_prompt( self, prompt):
        self.s.ALAnimatedSpeech.say(random.choice(SERVICE_PROMPTS[self.language][prompt]))

    @qi.bind(returnType=qi.Void, paramsType=[qi.String])
    def send(self, text):
        self.say_prompt('sending_prompt')
        self.logger.info('Chystam se poslat zpravu:' + text)
        data = {'msg': text, 'action': 'send'}
        #response = requests.post(GPT_SERVICE_URL, data=data)
        #response = requests.get(GPT_SERVICE_URL, params=data)
        response = http.request('GET', GPT_SERVICE_URL + '?action=send&msg='+text, timeout=TIMEOUT)
        #response_text = response.text.encode('utf-8')
        #response_text = response.read().encode('utf-8')
        self.logger.info('Response: ' + str(response))
        response_text = str(response.data)
        self.logger.info('Dostal jsem odpoved:' + response_text)
        self.say_prompt('received_prompt')
        self.s.ALAnimatedSpeech.say(response_text)
        return
    
    @qi.bind(returnType=qi.Void, paramsType=[])
    def new(self):
        self.logger.info('Mazu predchozi kontext konverzace')
        #requests.get(GPT_SERVICE_URL + '?action=new')
        http.request('GET', GPT_SERVICE_URL + '?action=new')

    @qi.nobind
    def on_start(self):
        pass

    @qi.bind(returnType=qi.Void, paramsType=[])
    def stop(self):
        "Stop the service."
        self.logger.info("ALGPTService stopped by user request.")
        self.qiapp.stop()

    @qi.nobind
    def on_stop(self):
        "Cleanup (add yours if needed)"
        self.logger.info("ALGPTService finished.")
        self.events.clear()

####################
# Setup and Run
####################

if __name__ == "__main__":
    if DEBUG_CONVERT:
        a = ALGPTService(None)
        a.compute()
        sys.exit(0)
    stk.runner.run_service(ALGPTService)

