MYAPPS_DIR=$(HOME)/pepper
BASE_DIR=/nlp/projekty/pepper
INSTALL_PKG=$(BASE_DIR)/bin/install_pkg.py

pkg:
	umask 002; \
	pkgname=$(wildcard *.pml); \
	if [ -z "$$pkgname" ]; then \
		echo "I need a .pml file here"; \
		exit 1; \
	fi; \
	basename="`basename -s .pml $$pkgname`"; \
	rm -fv $$basename*.pkg || true; \
	qipkg bump-version manifest.xml; \
	qipkg make-package $$pkgname; \
	mv -v $$basename*.pkg $(MYAPPS_DIR) || true

install:
	umask 002; \
	pkgname=$(wildcard *.pml); \
	if [ -z "$$pkgname" ]; then \
		echo "I need a .pml file here"; \
		exit 1; \
	fi; \
	basename="`basename -s .pml $$pkgname`"; \
	pkgfile="`ls -t $(MYAPPS_DIR)/$$basename*pkg|head -n1`"; \
	if [ -z "$$pkgfile" ]; then \
		echo "Package file for $$basename not found in $(MYAPPS_DIR)"; \
		exit 1; \
	fi; \
	$(INSTALL_PKG) $$pkgfile

