#!/usr/bin/python3.8

import requests
import json
import sys
import os

import logging

OPENAI_API_KEY = ''
OPENAI_MODEL = 'gpt-3.5-turbo'
SERVER_TITLE = 'GPT Command'
LOG_FILE = '/var/log/gpt_command.log'

import cgi
import cgitb
cgitb.enable(display=0, logdir="/path/to/logdir", format="text")

#logging.basicConfig(filename=LOG_FILE, level=logging.INFO,
logging.basicConfig(filename=LOG_FILE, level=logging.DEBUG,
    format='%(asctime)s %(levelname)s %(message)s')

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
    else:
        logging.critical("Exception occured:", exc_info=(exc_type, exc_value, exc_traceback))

sys.excepthook = handle_exception


class CHAT():
    def __init__(self, key, model=OPENAI_MODEL) -> None:
        self.url = "https://api.openai.com/v1/chat/completions"
        self.headers = {
            "Authorization": f"Bearer {key}",
            "Content-Type": "application/json"
        }   
        self.model = model
        self.messages = []
        
    def add_message(self, promt, role):
        self.messages.append({'role': role, 'content': promt})

    def get_request(self):
        return requests.post(
            self.url,
            headers=self.headers, 
            json={
                "model": self.model,
                "messages": self.messages,
                }
        )

    def send(self, promt, role='user'):
        self.add_message(promt, role)
        json = self.get_request().json()
        logging.info(f"Obtained chat response: {json}")
        msg = json['choices'][0]['message']['content']
        self.add_message(msg, 'assistant')
        return msg

chat = CHAT(OPENAI_API_KEY)
    
def history():
    msgs = ''
    for msg in chat.messages:
        msgs += msg['content']
        if msgs[-1] != '\n':
            msgs += '\n'
    return msgs

def reset_history():
    logging.info(f"History reset")
    chat.messages = []
    return ''

def send( msg):
    try:
        logging.info(f"Send text: '{msg}'")
        res = chat.send(msg)
    except Exception as e:
        logging.error(f"Error occured: {e}")
        return "Omlouvám se, při vymýšlení odpovědi se mi přehřály obvody."
    return res

def get_data():
    data = ""
    logging.debug(f"CONTENT_LENGTH = {os.environ.get('CONTENT_LENGTH')}")
    if int(os.environ.get('CONTENT_LENGTH', 0)) != 0:
        for i in range(int(os.environ.get('CONTENT_LENGTH', 0))):
            data += sys.stdin.read(1)
    return data

if __name__ == '__main__':
    print("Content-Type: text/plain; charset=utf-8\n")

    request = cgi.FieldStorage()
    data = get_data()
    logging.debug(f"Request: {request}")
    logging.debug(f"Request data: {data}")
    env = ''
    for key in os.environ:
        env += f"{key} = {os.environ[key]}\n"
    #logging.debug(f"Enviroment : {env}")
    action = request.getvalue('action')
    logging.info(f"Action: {action}")
    result = None
    if action == 'send':
        result = send(request.getvalue('msg'))
    elif action == 'history':
        result = history()
    elif action == 'new':
        result = reset_history()
    if result is not None:
        print(result)
        logging.info(f"Result: {result}")
    else:
        print("Unknown action")
        logging.info(f"Unknown action")
