#!/usr/bin/env python3.8

from flask import Flask, request

import requests
import json
import sys
import os

import argparse
import logging

OPENAI_API_KEY = ''
OPENAI_MODEL = 'gpt-3.5-turbo'
SERVER_TITLE = 'GPT Command Server'
SERVER_HOST = '127.0.0.1'
SERVER_PORT = 8007
RUN_FILE = '/var/run/gpt_command.pid'
LOG_FILE = '/var/log/gpt_command.log'

logging.basicConfig(filename=LOG_FILE, level=logging.INFO)

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
    else:
        logging.critical("Exception occured:", exc_info=(exc_type, exc_value, exc_traceback))

sys.excepthook = handle_exception


class CHAT():
    def __init__(self, key, model=OPENAI_MODEL) -> None:
        self.url = "https://api.openai.com/v1/chat/completions"
        self.headers = {
            "Authorization": f"Bearer {key}",
            "Content-Type": "application/json"
        }   
        self.model = model
        self.messages = []
        
    def add_message(self, promt, role):
        self.messages.append({'role': role, 'content': promt})

    def get_request(self):
        return requests.post(
            self.url,
            headers=self.headers, 
            json={
                "model": self.model,
                "messages": self.messages,
                }
        )

    def send(self, promt, role='user'):
        self.add_message(promt, role)
        msg = self.get_request().json()['choices'][0]['message']['content']
        self.add_message(msg, 'assistant')
        return msg

def get_server_pid():
    pid = None
    try:
        with open(RUN_FILE, 'r') as f:
            pid = f.read()
    except:
        pass
    return pid
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=SERVER_TITLE)
    parser.add_argument('--key', type=str, help='OpenAI API key')
    parser.add_argument('--status', action='store_true', help='Print server status')
    args = parser.parse_args()
    if args.key:
        OPENAI_API_KEY = args.key

    server_pid = get_server_pid()
    if args.status:
        if server_pid:
            print(f"{SERVER_TITLE} is running with PID {server_pid}")
        else:
            print(f"{SERVER_TITLE} is not running")
        sys.exit(0)

    if server_pid is None:
        try:
            pid = os.fork()
            if pid > 0:
                print(f"Running {SERVER_TITLE} in the background with PID %d" % pid)
                logging.info(f"Running {SERVER_TITLE} in the background with PID %d" % pid)
                with open(RUN_FILE, 'w') as f:
                    f.write(str(pid))
                sys.exit(0)
        except OSError as e:
            print >> sys.stderr, "fork failed: %d (%s)" % (e.errno, e.strerror)
            sys.exit(1)

        # Configure the child processes environment
        os.chdir("/")
        os.setsid()
        os.umask(0)

        # Execute in the background
        chat = CHAT(OPENAI_API_KEY)

        app = Flask(__name__)
        app.run(host=SERVER_HOST, port=SERVER_PORT)

        @app.route('/gpt/history')
        def history():
            msgs = ''
            for msg in chat.messages:
                msgs += msg['content']
                if msgs[-1] != '\n':
                    msgs += '\n'
            return msgs

        @app.route('/gpt/new')
        def hello():
            logging.info(f"History reset")
            chat.messages = []
            return ''

        @app.route('/gpt/msg', methods=['POST'])
        def send():
            try:
                value = request.form['msg']
                logging.info(f"Send text: '{value}'")
                res = chat.send(value)
            except Exception as e:
                logging.error(f"Error occured: {e}")
                return "Omlouvám se, při vymýšlení odpovědi se mi přehřály obvody."
            logging.info(f"Obtained result: '{res}'")
            return res

